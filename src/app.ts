// Enums
enum StartType {
    Empty = 0,
    Still = 1,
    Gosper = 2,
    Acorn = 3,
    Block = 4
}

enum CellType {
    InitDead = 0,
    Alive = 1,
    Dead = 2
}

// Consts
const neighbors: [number, number][] = [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]]
const timeout: number = 10;

//Vars
var xSize: number = 60;
var ySize: number = 25;
var startType: StartType = StartType.Empty;
var field: CellType[][] = new Array<CellType[]>(new Array<CellType>());
var isRunning: boolean = false;
var iterations: number = 0;
var timerId: number;
var levelsUrl: string;

// 'Cause the base '%' function is rubbish...
function mod(x: number, y: number): number {
    return ((x % y) + y) % y;
};



function generateBoard(y: number, x: number): CellType[][] {
    //   Create a 2-dim array filled with dead cells.                                   
    //                                                                                  make a deep copy of each row beacuse 
    //                                                                                  the fill method uses the same row 
    //                                                                                  object for each row
    return new Array<CellType[]>(y).fill(new Array<CellType>(x).fill(CellType.InitDead)).map(row => row.slice(0));
}

function resetFieldCallback(): void {
    if (isRunning) {
        startStopGame();
    }

    field = generateBoard(ySize, xSize);
    createViewBoard(field);

    iterations = 0;
    setCounter(iterations);
    //TODO: collapse menu
}

function setSizeCallback(): void {
    //TODO: maybe disable button
    if (isRunning) {
        startStopGame();
    }

    //TODO: maybe optimize
    const newY = (document.getElementById("ySize") as HTMLInputElement).valueAsNumber;
    const newX = (document.getElementById("xSize") as HTMLInputElement).valueAsNumber;
    let success = true;

    // TODO: check for integer????
    if (newY === NaN || newY === undefined || newY < 1) {
        success = false;
    }

    if (newX === NaN || newX === undefined || newX < 1) {
        success = false;
    }

    if (success) {
        ySize = newY;
        xSize = newX;

        field = generateBoard(ySize, xSize);
        createViewBoard(field);
        setCounter(0);
    } else {
        window.alert("Invalid board size!");
    }
}

function cellClickedCallback(sender: HTMLDivElement, yInd: number, xInd: number): void {
    if (sender.className === "cell-alive") {
        field[yInd][xInd] = CellType.InitDead;
        sender.className = "cell-initdead";
    }
    else {
        field[yInd][xInd] = CellType.Alive;
        sender.className = "cell-alive";
    }
}

function setCounter(count: number): void {
    document.getElementById("countBox").textContent = String(count);
}


function startStopGame(): void {
    if (isRunning) {
        isRunning = false;
        document.getElementById("startStopButton").textContent = "Start";

        clearInterval(timerId);
        timerId = null;
    }
    else {
        isRunning = true;
        document.getElementById("startStopButton").textContent = "Pause";

        timerId = window.setInterval(() => gameIterationCallback(), timeout);
    }
}

function gameIterationCallback(): void {
    field = updateField(field);
    updateDivs(getDivs(), field);
    ++iterations;
    setCounter(iterations);
}

//TODO: check input, don't save bad values.
/*
function checkInput(sender: HTMLInputElement): void {
    const numnum = parseInt(sender.value);

    if (numnum > 0) {
        
    } else {
        
    }
}*/

function resizeCallback(wait: boolean): void {
    if (wait) {
        resizeDivsAsync(1000);
    } else {
        resizeDivs();
    }
}

function resizeDivsAsync(waitTime: number) {
    setTimeout(() => {
        resizeDivs();
    }, waitTime);
}

function resizeDivs() {
    const size = `${calcCellSize(ySize, xSize)}px`;
    const divRows = getDivRows();

    divRows.map(row => {
        const rowElem = <HTMLElement>row;
        //rowElem.style.height = size;
        rowElem.style.width = "100%";
        rowElem.style.paddingBottom = "0px";

        [...row.children].map(div => {
            const divElem = <HTMLElement>div;
            divElem.style.paddingBottom = "0px";
            divElem.style.width = size;
            divElem.style.height = size;
            divElem.style.minWidth = size;
            divElem.style.minHeight = size;
            //divElem.style.maxWidth = size;
        })
    });

    /*
    const num = Math.min(100 / divRows.length, 100 / divRows[0].children.length);
    let size = `${num}%`;
    const maxHeight = document.body.offsetHeight - document.getElementById("navbar").offsetHeight - document.getElementById("counterField").offsetHeight;

    
    if (maxHeight < (divRows.length * (num / 100) * maxHeight)) {
        size = `${maxHeight / divRows.length}px`;
    }

    */
}


function getDivs(): Element[][] {
    return [...document.getElementById("gameField").children].map(line => [...line.children]);
}

function getDivRows(): Element[] {
    return [...document.getElementById("gameField").children];
}

function updateDivs(elems: Element[][], fields: CellType[][]): void {
    elems.map((row, ri) => row.map((cell, ci) => {
        switch (fields[ri][ci]) {
            case CellType.InitDead:
                cell.className = "cell-initdead";
                break;
            case CellType.Alive:
                cell.className = "cell-alive";
                break;
            case CellType.Dead:
                cell.className = "cell-dead";
                break;
            default:
                console.log("Invalid celltype in updatedivs!");
                break;
        }
    }));

}

function calcCellSize(tableHeight: number, tableWidth: number): number {
    const width = document.getElementById("gameFieldRow").offsetWidth / tableWidth;
    const height = (document.body.offsetHeight
        - document.getElementById("navbar").offsetHeight
        - document.getElementById("counterField").offsetHeight
    ) / tableHeight;

    return Math.min(width, height) * 0.989;
}

// Creates a board in the HTML and deletes existing children.
function createViewBoard(board: CellType[][]): void {
    const root = document.getElementById("gameField");
    /*
        if (wheight > wwidth) {
            cellSize = `${Math.min(100 / board.length, 100 / board[0].length)}%`;
        } else {
            cellSize = `${Math.min(100 / board.length, 100 / board[0].length)}%`;
        }
    */
    /*switch ([(height > width), (wheight > wwidth)]) {
        case [true, true]:
            cellSize = `${width}%`;
            break;
        case [true, false]:
            cellSize = `${width}%`;

            break;
        case [false, true]:

            break;
        default:
            break;
    }*/

    const size = `${calcCellSize(board.length, board[0].length) * 0.95}px`;

    //const cellSize = `${Math.min(100 / board.length, 100 / board[0].length)}%`;
    // clear children
    root.innerHTML = "";

    board.map((row, ri) => {
        const rowDiv = document.createElement("div");
        rowDiv.className = "boardRow";
        //rowDiv.style.height = cellSize;
        //rowDiv.style.width = "100%";

        row.map((cell, ci) => {
            const cellDiv = document.createElement("div");
            cellDiv.style.width = size;
            cellDiv.style.paddingBottom = size;

            switch (cell) {
                case CellType.InitDead:
                    cellDiv.className = "cell-initdead";
                    break;
                case CellType.Alive:
                    cellDiv.className = "cell-alive";
                    break;
                case CellType.Dead:
                    cellDiv.className = "cell-dead";
                    break;
                default:
                    console.log("Got invalid enum element!");
                    break;
            }

            cellDiv.addEventListener("click", () => cellClickedCallback(cellDiv, ri, ci));
            rowDiv.appendChild(cellDiv);
        });

        root.appendChild(rowDiv);
    });
}

function updateField(arr: CellType[][]): CellType[][] {
    //TODO: delete first ()
    //                      clone array       map calculation of new cell on each cell
    return (arr.map(x => x.slice(0))).map((row, ri) => row.map((cell, ci) => calcCell(field, cell, ri, ci)));
}

// Calculates the value of a cell (used in an iteration).
function calcCell(arr: CellType[][], currentVal: CellType, y: number, x: number): CellType {
    //let neighborCount = 0;
    const arrHeight = arr.length;
    const arrWidth = arr[0].length;

    let neighborCount = neighbors.reduce(((acc, neighbor) => {
        let yInd = mod(y + neighbor[0], arrHeight);
        let xInd = mod(x + neighbor[1], arrWidth);

        if (arr[yInd][xInd] === CellType.Alive) {
            ++acc;
        }

        return acc;
    }), 0);

    //TODO: check if you should do it with .valueof
    if (currentVal === CellType.Alive) {
        // Rule 1
        if (neighborCount < 2) {
            return CellType.Dead;
        }

        // Rule 3
        if (neighborCount > 3) {
            return CellType.Dead;
        }

        // Rule 2
        return CellType.Alive;

    }

    // Rule 4
    if (currentVal.valueOf() !== CellType.Alive.valueOf() && neighborCount === 3) {
        return CellType.Alive;
    }

    return currentVal;
}


function setSizeView(): void {
    (document.getElementById("ySize") as HTMLInputElement).valueAsNumber = ySize;
    (document.getElementById("xSize") as HTMLInputElement).valueAsNumber = xSize;
}


function getLevelsRemoteAsync(callback: ((number, string) => void)): void {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", levelsUrl, true);
    //xhr.responseType = "text";
    xhr.onload = (() => {
        callback(xhr.status, xhr.responseText);
    });

    try {
        xhr.send();
    } catch (error) {
        window.alert("Could not connect to the server!");
    }
}

function getLevelsRemoteCallback(statusCode: number, response: string): void {
    if (statusCode === 200 && response != null && response != undefined) {
        var err = false;
        let levels: string[];

        try {
            levels = JSON.parse(response);
        } catch (error) {
            console.log("Response was malformed json!");
            err = true;
        }

        if (!err) {
            const levelsContainer = document.getElementById("levelsContainer");

            levels.forEach(level => {
                const levelDiv = document.createElement("li");
                levelDiv.className = "dropdown-item";
                levelDiv.textContent = level;

                levelDiv.addEventListener("click", () => getLevelContentRemoteAsync(getLevelContentRemoteCallback, level));
                levelsContainer.appendChild(levelDiv);
            });

            return;
        }
    }

    window.alert("The website was unable to load the levels!\nThis function might be temporary unavailable!");
}

function getLevelContentRemoteAsync(callback: ((number, string) => void), level: string): void {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", levelsUrl + `/${level}`, true);
    xhr.onload = (() => {
        callback(xhr.status, xhr.responseText);
    });

    try {
        xhr.send();
    } catch (error) {
        window.alert("Could not connect to the server!");
    }
}

function getLevelContentRemoteCallback(statusCode: number, response: string): void {
    if (statusCode === 200 && response != null && response != undefined) {
        var err = false;
        let lines: string[];

        try {
            lines = JSON.parse(response)[1];
        } catch (error) {
            console.log("Response was malformed json!");
            err = true;
        }

        if (!err) {
            const levelsContainer = document.getElementById("levelsContainer");
            const newField: CellType[][] = lines.map(line => line.split("").map(cell => Number(cell)));

            if (newField.length > 0 && newField[0].length > 0) {
                if (isRunning) {
                    startStopGame();
                }

                iterations = 0;
                setCounter(iterations);
                ySize = newField.length;
                xSize = newField[0].length;

                field = newField;
                createViewBoard(field);

                return;
            }
        }
    }

    window.alert("The website was unable to load this level!\nThis function might be temporary unavailable!");
}


function initApp(): void {
    levelsUrl = new URL("/api/levels", document.URL).href;

    setCounter(iterations);

    let startButton = document.getElementById("startStopButton");
    startButton.textContent = "Start";
    startButton.addEventListener("click", startStopGame);

    document.getElementById("setSizeButton").addEventListener("click", setSizeCallback);
    document.getElementById("resetButton").addEventListener("click", resetFieldCallback);
    document.getElementById("hideHudButton").addEventListener("click", () => resizeCallback(true));
    document.getElementById("hideMenuButton").addEventListener("click", () => resizeCallback(true));
    document.getElementById("hideMenuSpan").addEventListener("click", () => resizeCallback(true));
    document.getElementById("levelsDropdownItem").addEventListener("click", () => {
        if (document.body.offsetWidth < 768) resizeCallback(true);
    });
    document.getElementById("sizeDropdownItem").addEventListener("click", () => {
        if (document.body.offsetWidth < 768) resizeCallback(true);
    });
    document.getElementById("navbarDropdownMenuSize").addEventListener("click", () => {
        if (document.body.offsetWidth < 768) resizeCallback(true);
    });
    document.getElementById("navbarDropdownMenuLink").addEventListener("click", () => {
        if (document.body.offsetWidth < 768) resizeCallback(true);
    });
    //document.body.offsetWidth

    field = generateBoard(ySize, xSize);

    createViewBoard(field);

    document.getElementById("navbarDropdownMenuSize").addEventListener("click", setSizeView);
    setSizeView();

    getLevelsRemoteAsync(getLevelsRemoteCallback);

    //document.getElementById("gameField").addEventListener("resize", resizeCallback);
    window.addEventListener("resize", () => resizeCallback(false));
}

window.onload = (() => initApp());