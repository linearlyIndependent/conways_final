module.exports = function (grunt) {
	require('google-closure-compiler').grunt(grunt);

	grunt.initConfig({
		watch: {
			src: {
				tasks: ['checkInternal'],
				files: ['src/**/*.*']
			}
		},
		
		browserSync: {
			bsFiles: {
				src : ['./dist/meta.json']
			},
			options: {
				logLevel: 'debug',
				logConnections: true,
				logFileChanges: true,
				server: './',
				watchEvents: ['add', 'change']
			}
		},
		vnuserver: {},
		htmllint: {
			src: {
				options: {
					server: {}
				},
				src: 'src/**/*.html'
			},
			dist: {
				options: {
					server: {}
				},
				src: 'dist/**/*.html'
			}
		},
		htmlmin: {
			dist: {
				options: {
					removeComments: true,
					collapseWhitespace: true
				},
				files: {
					'dist/index.html': 'src/*.html',
				}
			}
		},
		'w3c_css_validation': {
			dist: {
				options: {
					logfile: './logs/w3c_css_validation.json'
				},
				src: ['src/**/*.css', 'dist/**/*.css'],
			}
		},
		'closure-compiler': {
			dist: {
				files: {
					'dist/app.min.js': ['src/**/*.js']
				},
				options: {
					//debug: true,
					//formatting: 'PRETTY_PRINT',
					compilation_level: 'SIMPLE',
					language_in: 'ECMASCRIPT6_STRICT',
					warningLevel: "VERBOSE"
				}
			}
		},
		ts: {
			default : {
				src: "src/**/*.ts",
				out: 'dist/tsapp.min.js',
				options: {
					module: 'amd',
					target: 'es6',
					esModuleInterop: true
				}
			}
		},
		sass: {
			dist: {
				src: 'src/*.scss',
				dest: 'dist/style.css'
			}
		},
		"copy": {
			"main": {
				"files": [
					{
						expand: true, 
						cwd: 'src/', 
						src: ['*.ico'], 
						dest: 'dist/', 
						filter: 'isFile'
					}
				]
			}
		},
		"file-creator": {
			"meta": {
				"dist/meta.json": function(fs, fd, done) {
					fs.writeSync(fd, 
`{ 
	"meta": { 
		"build-time": "${new Date().toISOString()}"
	}
}`);
					done();
				}
			}
		}
	});

	// loaded npm packages
	grunt.loadNpmTasks('grunt-vnuserver');
	grunt.loadNpmTasks('grunt-html');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-w3c-css-validation');
	grunt.loadNpmTasks('grunt-node-sass');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-ts');
	grunt.loadNpmTasks('grunt-file-creator');
	grunt.loadNpmTasks('grunt-contrib-copy');

	// registered tasks
	grunt.registerTask('checkInternal', ['htmlmin', 'htmllint', 'sass', 'w3c_css_validation', 'ts', 'closure-compiler', 'copy', 'file-creator']);
	grunt.registerTask('default', ['vnuserver', 'checkInternal', 'watch']);
};