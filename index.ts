import * as express from "express";
import * as path from "path";
import * as fs from "fs";
import { Promise } from 'es6-promise';

export class Server {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.app.use(express.static(path.join(__dirname, "dist")));  // http://expressjs.com/en/starter/static-files.html
        this.app.use(this.logRequest);                              // http://expressjs.com/en/guide/writing-middleware.html
        this.app.get('/api/levels/:levelname', this.levelContentEPHandler);
        this.app.get('/api/levels', this.levelsEPHandler);
        this.app.listen(3001);
    }

    private levelsEPHandler(req: express.Request, res: express.Response) {
        var prom = new Promise((resolve, reject) => {
            return fs.readdir(path.join(__dirname, "dist", "levels"), (err, files) => {
                if (err) {
                    return reject(err);
                } else {
                    const filtered = files.filter(file => path.extname(file).toLowerCase() === ".txt")
                        .map(name => name.slice(0, -4));

                    return resolve(JSON.stringify(filtered));
                }
            })
        });

        Promise.race([prom])
            .then(results => res.send(results))
            .catch(err => res.status(500).send("Oh snap, this function encountered an error."));
    }

    private readLevels(req: express.Request, res: express.Response) {
        return new Promise((resolve, reject) => {
            return fs.readdir(path.join(__dirname, "dist", "levels"), (err, files) => {
                if (err) {
                    return reject(err);
                } else {
                    const filtered = files.filter(file => path.extname(file).toLowerCase() === ".txt")
                        .map(name => name.slice(0, -4));

                    return resolve(JSON.stringify(filtered));
                }
            })
        });
    }

    private levelContentEPHandler(req: express.Request, res: express.Response) {
        var prom = new Promise((resolve, reject) => {
            let fileName: string;

            if (!req.params || !req.params.levelname || typeof req.params.levelname !== "string") {
                return reject(400);
            } else {
                fileName = (String(req.params.levelname) + ".txt");
            }

            return fs.readFile(path.join(__dirname, "dist", "levels", encodeURIComponent(fileName)), "utf8", (err, data) => {
                if (err) {
                    return reject(404);
                } else {
                    const trimmed = data.trim().split("\n")
                        .map(line => line.trim())
                        .filter(line => line !== "");

                    return resolve(JSON.stringify([fileName, trimmed]));
                }
            })
        });

        Promise.race([prom])
            .then(results => res.send(results))
            .catch(err => {
                let respCode;

                if (typeof err == "number") {
                    respCode = +err;
                } else {
                    respCode = 500;
                }

                //TODO: do i need error messages too?
                res.status(respCode).send();
            });//,"Internal Server Error"));
    }


    private logRequest(req: express.Request, res: express.Response, next: express.NextFunction) {
        const date = new Date();
        console.log(`${date.toISOString()} | ${req.ip} -> ${req.method}: ${req.url}`);
        next();
    }
}

new Server();

console.log("************************************\nServer is running at 127.0.0.1:3001\n************************************\n");
console.log("<datetime(ISO)> | <ip> -> <method>: <url>\n\n");